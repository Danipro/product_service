package com.danipro.productservice.services;

import com.danipro.productservice.dao.dto.ProductRequest;
import com.danipro.productservice.dao.dto.ProductResponse;
import com.danipro.productservice.dao.models.Product;
import com.danipro.productservice.dao.repository.ProductRepository;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.stream.Collectors;

@Service
@RequiredArgsConstructor
@Slf4j
public class ProductService {

    private final ProductRepository productRepository;

    public void createProduct(ProductRequest request){
        Product product = Product.builder()
                .name(request.getName())
                .description(request.getDescription())
                .price(request.getPrice())
                .build();
        this.productRepository.save(product);
        log.info("Product {} to be save ",product);
    }

    public List<ProductResponse> getAllProducts() {
        List<Product> products = productRepository.findAll();

     return  products.stream().map(this::mapToProductResponse).collect(Collectors.toList());
    }

    private ProductResponse mapToProductResponse(Product product) {
        return ProductResponse.builder()
                .id(product.getId())
                .name(product.getName())
                .description(product.getDescription())
                .price(product.getPrice()).build();
    }
}
