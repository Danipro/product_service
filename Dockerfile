FROM maven:3-jdk-11 AS build

WORKDIR /opt/app

COPY ./ /opt/app

RUN mvn install -DskipTests

FROM openjdk:11


COPY --from=build /opt/app/target/*.jar app.jar

ENV PORT 7000
EXPOSE $PORT

ENTRYPOINT ["java","-jar","-Xmx1024M","-Dserver.port=${PORT}","app.jar"]